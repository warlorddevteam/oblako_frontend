import { LaststendPage } from './app.po';

describe('laststend App', () => {
  let page: LaststendPage;

  beforeEach(() => {
    page = new LaststendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
