import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { User } from 'app/entity/user';
import { Response} from '@angular/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserService],
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService
      .getData()
      .subscribe((resp: Response) => this.users = resp.json());
  }
}
