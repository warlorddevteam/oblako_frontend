import { Component, OnInit } from '@angular/core';
import {Declaration} from '../../entity/declaration';
import {DeclarationService} from '../../services/declaration.service';
import { Response} from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import {Alco} from "../../entity/alco";

const URL = 'http://localhost:8000/alco/upload';


@Component({
  selector: 'app-declaretion',
  providers: [DeclarationService],
  templateUrl: './declaretion.component.html',
  styleUrls: ['./declaretion.component.css'],
})
export class DeclarationComponent implements OnInit {
  public  declarations: Alco[] = [];
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;

  public settings = {
    columns: {
      id: {
        title: 'ID'
      },
      SERTIF: {
        title: 'Алко код'
      },
      NAME: {
        title: 'id магазина'
      },
      LKVP: {
        title: 'Фсрар'
      },
      GOST: {
        title: 'Количество'
      },
      COMP_NID: {
        title: 'Количество'
      },
      DICT_ID: {
        title: 'Количество'
      },
      CAPACITY: {
        title: 'Количество'
      },
      DATE_ROZ: {
        title: 'Количество'
      },
      UKP: {
        title: 'Количество'
      },
      UKP1: {
        title: 'Количество'
      },
      GRAD_S: {
        title: 'Количество'
      },
      PROD_ID: {
        title: 'Количество'
      },
    }
  };
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
  constructor(private declarationService: DeclarationService) {}
  ngOnInit() {
    this.
      declarationService.getData()
      .subscribe((resp: Response) => this.declarations = resp.json());
  }

}