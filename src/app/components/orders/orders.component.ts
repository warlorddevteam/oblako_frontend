import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'app/services/orders.service';
import { Order } from 'app/entity/order';
import { Response} from '@angular/http';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [OrdersService],
})
export class OrdersComponent implements OnInit {

 orders: Order[] = [];
   constructor(private ordersService: OrdersService) {}

  ngOnInit() {
   this.ordersService
   .getData()
   .subscribe((resp: Response) => this.orders = resp.json());
  }

}
