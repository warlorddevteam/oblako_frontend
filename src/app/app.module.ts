import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { OrdersComponent } from './components/orders/orders.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { ChatComponent } from './components/chat/chat.component';
import { UsersComponent } from './components/users/users.component';
import { DeclarationComponent } from './components/declaretion/declaretion.component';
import { FileUploadModule } from 'ng2-file-upload';
import {Ng2SmartTableModule} from "ng2-smart-table";

// Rooutes
const appRoutes: Routes = [
  {path: '', component: TasksComponent},
  {path: 'users', component: UsersComponent},
  {path: 'declaration', component: DeclarationComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    TasksComponent,
    AnalyticsComponent,
    ChatComponent,
    UsersComponent,
    DeclarationComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FileUploadModule,
    FormsModule,
    Ng2SmartTableModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
