export class Declaration {
  id: number;
  Quantity: String;
  AlcCode: String;
  FSRAR: String;
  RestsShopMini_id: String;
}
