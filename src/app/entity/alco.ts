export class Alco {
   id: Number;
   SERTIF: String;
   NAME: String;
   LKVP: String;
   GOST: String;
   COMP_NID: String;
   DICT_ID: String;
   CAPACITY: String;
   DATE_ROZ: String;
   UKP: String;
   UKP1: String;
   GRAD_S: String;
   PROD_ID: String;
}