export class User {
  id: string;
  login: string;
  password: string;
  firstname: string;
  lastName: string;
  middleName: string;
  role: number;
  departament: number;
  active: boolean;
}
