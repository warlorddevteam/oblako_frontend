import { TestBed, inject } from '@angular/core/testing';

import { DeclarationService } from './declaration.service';

describe('DeclarationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeclarationService]
    });
  });

  it('should ...', inject([DeclarationService], (service: DeclarationService) => {
    expect(service).toBeTruthy();
  }));
});
