import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';

@Injectable()
export class UserService {

  constructor(private http: Http) {}

  getData() {
    return this.http.get('http://localhost:8000/user/all');
  }
}
